﻿using AgenciaApiEntity.Common.DataBase.Classes;
using AgenciaApiEntity.ViajerosModels.Class;
using AgenciaApiEntity.ViajerosModels.Interface.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace AgenciaApiDataAccess.ViajerosModels
{
    public class DataViajero : IDataViajero
    {
        readonly DbEntityContext context;

        public DataViajero(DbEntityContext context)
        {
            this.context = context;
        }

        public async Task<bool> CreateViajero(Viajero viajero)
        {
            try
            {
                context.Viajeros.Add(viajero);

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> DeleteViajero(int id)
        {
            try
            {
                var model = await context.Viajeros.FindAsync(id);

                context.Viajeros.Remove(model);

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<Viajero> GetViajero(int id)
        {
            try
            {
                return await context.Viajeros.FindAsync(id);
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<Viajero> GetViajeroPorCedula(string cedula)
        {
            try
            {
                return await context.Viajeros.Where(x => x.Cedula == cedula).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<List<Viajero>> GetViajeros()
        {
            try
            {
                // Retorna todos los viajeros disponibles comunmente es necesario implementar un metodo de paginacion
                return await context.Viajeros.ToListAsync();
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> UpdateViajero(Viajero viajero)
        {
            try
            {

                var updateModel = await context.Viajeros.FindAsync(viajero.Id);
         
                updateModel.Nombre = viajero.Nombre;
                updateModel.Telefono = viajero.Telefono;
                updateModel.Cedula = viajero.Cedula;

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }
    }
}
