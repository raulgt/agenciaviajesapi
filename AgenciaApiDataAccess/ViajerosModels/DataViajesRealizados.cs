﻿using AgenciaApiEntity.ViajerosModels.Interface.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using AgenciaApiEntity.ViajerosModels.Class;
using System.Threading.Tasks;
using AgenciaApiEntity.Common.DataBase.Classes;
using Microsoft.EntityFrameworkCore;
using AgenciaApiEntity.ViajerosModels.AuxiliarModels;

namespace AgenciaApiDataAccess.ViajerosModels
{
    public class DataViajesRealizados : IDataViajesRealizados
    {
        readonly DbEntityContext context;

        public DataViajesRealizados(DbEntityContext context)
        {
            this.context = context;
        }

        public async Task<bool> CreateViajeRealizado(ViajesRealizados viajesRealizados)
        {
            try
            {
                viajesRealizados.FechaRegistro = DateTime.Now;

                context.ViajesRealizados.Add(viajesRealizados);

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> DeleteViajeRealizado(int id)
        {
            try
            {
                var model = await context.ViajesRealizados.FindAsync(id);

                context.ViajesRealizados.Remove(model);

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }

        public async Task<List<ViajesCompletos>> GetViajesRealizados()
        {
            try
            {
                return await Task.Run(() =>
                {

                    // Posiblemente sea necesario implementar una extrategia de paginacion si la lista es grande
                    var resp = (from vr in context.ViajesRealizados
                                join vo in context.Viajeros
                                on vr.ViajeroId equals vo.Id
                                join ve in context.Viajes
                                on vr.ViajeId equals ve.Id
                                select new ViajesCompletos
                                {
                                    Id = vr.Id,
                                    Nombre = vo.Nombre,
                                    Cedula = vo.Cedula,
                                    Telefono = vo.Telefono,
                                    CodigoViaje = ve.Codigo,
                                    Origen = ve.Origen,
                                    Destino = ve.Destino,
                                    Precio = ve.Precio
                                });

                    return resp.ToList();
                });
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }


        }

        public async Task<ViajesRealizados> GetViajesRealizadosPorId(int id)
        {
            try
            {
                 var resp = await context.ViajesRealizados.FindAsync(id);

                 return resp;
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }

        public async Task<List<ViajesCompletos>> GetViajesRealizadosPorViajero(string cedula)
        {
            try
            {
                return await Task.Run(() =>
                {

                    var resp = (from vr in context.ViajesRealizados
                                join vo in context.Viajeros
                                on vr.ViajeroId equals vo.Id
                                join ve in context.Viajes
                                on vr.ViajeId equals ve.Id
                                where vo.Cedula == cedula
                                select new ViajesCompletos
                                {
                                    Id = vr.Id,
                                    Nombre = vo.Nombre,
                                    Cedula = vo.Cedula,
                                    Telefono = vo.Telefono,
                                    CodigoViaje = ve.Codigo,
                                    Origen = ve.Origen,
                                    Destino = ve.Destino,
                                    Precio = ve.Precio
                                });

                    return resp.ToList();
                });
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> UpdateViajeRealizado(ViajesRealizados viajesRealizados)
        {
            try
            {
                var modelUpdate = await context.ViajesRealizados.FindAsync(viajesRealizados.Id);
                modelUpdate.ViajeId = viajesRealizados.ViajeId;
                modelUpdate.ViajeroId = viajesRealizados.ViajeroId;            
                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }
    }
}
