﻿using AgenciaApiEntity.Common.DataBase.Classes;
using AgenciaApiEntity.ViajesModels.Class;
using AgenciaApiEntity.ViajesModels.Interface.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AgenciaApiDataAccess.ViajesModels
{
    public class DataViaje : IDataViaje
    {
        readonly DbEntityContext context;

        public DataViaje(DbEntityContext context)
        {
            this.context = context;
        }

        public async Task<bool> CreateViaje(Viaje viaje)
        {
            try
            {
                context.Viajes.Add(viaje);               

               var result =  await context.SaveChangesAsync();

               return result > 0;
            }
            catch (Exception e)
            {

                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> DeleteViaje(int id)
        {
            try
            {
                var model = await context.Viajes.FindAsync(id);

                context.Viajes.Remove(model);

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<Viaje> GetViaje(int id)
        {
            try
            {
                return await context.Viajes.FindAsync(id);
                
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<List<Viaje>> GetViajes()
        {
            try
            {
                // Retorna todos los viajes disponibles comunmente es necesario implementar un metodo de paginacion
                return await context.Viajes.ToListAsync();
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> UpdateViaje(Viaje viaje)
        {
            try
            {                              
                context.Attach(viaje);
                context.Entry(viaje).State = EntityState.Modified;           

                var result = await context.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            } 
        }
    }
}
