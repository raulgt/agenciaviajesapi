﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgenciaApiBussines.ViajerosModels;
using AgenciaApiBussines.ViajesModels;
using AgenciaApiDataAccess.ViajerosModels;
using AgenciaApiDataAccess.ViajesModels;
using AgenciaApiEntity;
using AgenciaApiEntity.Common.DataBase.Classes;
using AgenciaApiEntity.ViajerosModels.Interface.Data;
using AgenciaApiEntity.ViajerosModels.Interface.Service;
using AgenciaApiEntity.ViajesModels.Interface.Data;
using AgenciaApiEntity.ViajesModels.Interface.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AgenciaApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DbEntityContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("AgenciaApi")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<IOperationResult, OperationResult>();

            services.AddScoped<IDataViaje, DataViaje>();
            services.AddScoped<IServiceViaje, ServiceViaje>();


            services.AddScoped<IDataViajero, DataViajero>();
            services.AddScoped<IServiceViajero, ServiceViajero>();

            services.AddScoped<IDataViajesRealizados, DataViajesRealizados>();
            services.AddScoped<IServiceViajesRealizados, ServiceViajesRealizados>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
