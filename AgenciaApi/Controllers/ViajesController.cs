﻿using AgenciaApiEntity;
using AgenciaApiEntity.ViajesModels.Class;
using AgenciaApiEntity.ViajesModels.Interface.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ViajesController : ControllerBase
    {

        private readonly IServiceViaje serviceViajes;
        private readonly IOperationResult op;

        public ViajesController(IServiceViaje serviceViajes, IOperationResult op)
        {
            this.op = op;
            this.serviceViajes = serviceViajes;
        }


        [HttpGet]
        public async Task<IOperationResult> GetViajes()
        {
            return await serviceViajes.GetViajes();
        }

        [HttpGet("{id}")]
        public async Task<IOperationResult> GetViaje(int id)
        {
            return await serviceViajes.GetViaje(id);
        }

        [HttpPost]
        public async Task<IOperationResult> CreateViaje([FromBody] Viaje viaje) 
        {
            return await serviceViajes.CreateViaje(viaje);
        }

        [HttpDelete("{id}")]
        public async Task<IOperationResult> DeleteViaje(int id)
        {
            return await serviceViajes.DeleteViaje(id);
        }

        [HttpPut]
        public async Task<IOperationResult> UpdateViaje([FromBody] Viaje viaje) 
        {
            return await serviceViajes.UpdateViaje(viaje);
        }

    }
}
