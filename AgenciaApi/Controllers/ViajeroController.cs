﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgenciaApiEntity;
using AgenciaApiEntity.ViajerosModels.Class;
using AgenciaApiEntity.ViajerosModels.Interface.Service;
using Microsoft.AspNetCore.Mvc;

namespace AgenciaApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ViajeroController : ControllerBase
    {
        private readonly IServiceViajero serviceViajero;
        private readonly IOperationResult op;

        public ViajeroController(IServiceViajero serviceViajero, IOperationResult op)
        {
            this.op = op;
            this.serviceViajero = serviceViajero;
        }



        [HttpGet]
        public async Task<IOperationResult> GetViajeros()
        {
            return await serviceViajero.GetViajeros();
        }

  
        [HttpGet("{cedula}/GetViajeroPorCedula")] 
        public async Task<IOperationResult> GetViajeroPorCedula(string cedula)
        {
            return await serviceViajero.GetViajeroPorCedula(cedula);
        }


        [HttpGet("{id}/GetViajero")]
       // [Route("api/Viajero/{id}/GetViajero")]
        public async Task<IOperationResult> GetViajero(int id)
        {
            return await serviceViajero.GetViajero(id);
        }

      
        [HttpPost]
        public async Task<IOperationResult> CreateViajero([FromBody] Viajero viajero)
        {
            return await serviceViajero.CreateViajero(viajero);
        }

       
        [HttpDelete("{id}")]
        public async Task<IOperationResult> DeleteViajero(int id)
        {
            return await serviceViajero.DeleteViajero(id);
        }

       
        [HttpPut]
        public async Task<IOperationResult> UpdateViajero([FromBody] Viajero viajero)
        {
            return await serviceViajero.UpdateViajero(viajero);
        }
    }
}
