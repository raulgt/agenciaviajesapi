﻿using AgenciaApiEntity;
using AgenciaApiEntity.ViajerosModels.Class;
using AgenciaApiEntity.ViajerosModels.Interface.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ViajesRealizadosController : ControllerBase
    {

        private readonly IServiceViajesRealizados serviceViajesRealizados;
        private readonly IOperationResult op;

        public ViajesRealizadosController( IServiceViajesRealizados serviceViajesRealizados, IOperationResult op)
        {
            this.op = op;
            this.serviceViajesRealizados = serviceViajesRealizados;
        }


        [HttpGet]
        public async Task<IOperationResult> GetViajesRealizados()
        {
            return await serviceViajesRealizados.GetViajesRealizados();
        }

        [HttpGet("{cedula}")]
        public async Task<IOperationResult> GetViajesRealizadosPorViajero(string cedula)
        {
            return await serviceViajesRealizados.GetViajesRealizadosPorViajero(cedula);
        }

        [HttpGet("{id}/GetById")]
        public async Task<IOperationResult> GetViajesRealizadosPorId(int id)
        {
            return await serviceViajesRealizados.GetViajesRealizadosPorId(id);
        }


        [HttpPost]
        public async Task<IOperationResult> CreateViajeRealizado([FromBody] ViajesRealizados viajesRealizados)
        {
            return await serviceViajesRealizados.CreateViajeRealizado(viajesRealizados);
        }


        [HttpDelete("{id}")]
        public async Task<IOperationResult> DeleteViajeRealizado(int id)
        {
            return await serviceViajesRealizados.DeleteViajeRealizado(id);
        }

        [HttpPut]
        public async Task<IOperationResult> UpdateViajeRealizado([FromBody] ViajesRealizados viajesRealizados)
        {
            return await serviceViajesRealizados.UpdateViajeRealizado(viajesRealizados);
        }
    }
}
