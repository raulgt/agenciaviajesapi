### Codigo fuente del proyecto Servicio Rest para Agencia de Viajes

```
Pasos a seguir para ejecutar el proyecto en modo de desarrollo luego de clonar el repositorio: 

Crear una base de datos vacia en el servidor de SqlServer que se piensa utilizar llamada "AgenciaViajes".
Cheaquear el ConnectionStrings en el archivo "appsettings.json" dentro del proyecto "AgenciaApi" modificar el nombre del servidor de base
de datos de ser necesario.

Ejecutar los siguientes comandos en orden en el Packeg Manager Console: 
1) add-migration firtsMigration -o "DataMigrations"
2) update-database

Ejecute el proyecto en IIS Express
Luego de estos pasos el servicio rest esta listo para ser utilizado..!!
```