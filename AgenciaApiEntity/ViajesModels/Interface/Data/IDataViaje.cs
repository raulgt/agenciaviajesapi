﻿using AgenciaApiEntity.ViajesModels.Class;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiEntity.ViajesModels.Interface.Data
{
    public interface IDataViaje
    {
        Task<List<Viaje>> GetViajes();

        Task<Viaje> GetViaje(int id);

        Task<bool> CreateViaje(Viaje viaje);

        Task<bool> DeleteViaje(int id);

        Task<bool> UpdateViaje(Viaje viaje);
    }
}
