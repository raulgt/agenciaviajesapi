﻿using AgenciaApiEntity.ViajesModels.Class;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiEntity.ViajesModels.Interface.Service
{
    public interface IServiceViaje
    {
        Task<IOperationResult> GetViajes();

        Task<IOperationResult> GetViaje(int id);

        Task<IOperationResult> CreateViaje(Viaje viaje);

        Task<IOperationResult> DeleteViaje(int id);

        Task<IOperationResult> UpdateViaje(Viaje viaje);
    }
}
