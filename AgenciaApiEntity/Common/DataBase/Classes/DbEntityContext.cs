﻿

using AgenciaApiEntity.ViajerosModels.Class;
using AgenciaApiEntity.ViajesModels.Class;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgenciaApiEntity.Common.DataBase.Classes
{
    public class DbEntityContext : DbContext
    {

        public DbEntityContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Definimos el join entre los modelos ViajesRealizados - Viajes - Viajero            

            // Evitamos crear una clave primaria con las claves foraneas para permitir que un viajero pueda repetir viajes
            //builder.Entity<ViajesRealizados>()
            //    .HasKey(vr => new { vr.ViajeroId, vr.ViajeId });

            builder.Entity<ViajesRealizados>()
              .HasKey(vr => vr.Id);

            builder.Entity<ViajesRealizados>()
                .HasOne(vr => vr.Viajero)
                .WithMany(v => v.ViajesRealizados)
                .HasForeignKey(vr => vr.ViajeroId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<ViajesRealizados>()
                .HasOne(vr => vr.Viaje)
                .WithMany(v => v.ViajesRealizados)
                .HasForeignKey(vr => vr.ViajeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Viajero>()
                .HasIndex(u => u.Cedula)
                .IsUnique();

            builder.Entity<Viaje>()
               .HasIndex(u => u.Codigo)
               .IsUnique();


            base.OnModelCreating(builder);
        }


        public DbSet<Viaje> Viajes { get; set; }
        public DbSet<Viajero> Viajeros { get; set; }
        public DbSet<ViajesRealizados> ViajesRealizados { get; set; }


    }
}
