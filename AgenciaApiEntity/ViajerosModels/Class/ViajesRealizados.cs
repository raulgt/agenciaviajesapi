﻿using AgenciaApiEntity.ViajesModels.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AgenciaApiEntity.ViajerosModels.Class
{
    public class ViajesRealizados
    {
        // Agregamos un Id como primary Key ya que un viajero puede realizar varias veces el mismo viaje
        public int Id { get; set; }

        public int ViajeroId { get; set; }

        public Viajero Viajero  { get; set; }

        public int ViajeId { get; set; }

        public Viaje  Viaje { get; set; }                
       
        public DateTime FechaRegistro { get; set; }
       
    }
}
