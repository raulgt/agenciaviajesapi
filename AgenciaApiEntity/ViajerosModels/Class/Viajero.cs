﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace AgenciaApiEntity.ViajerosModels.Class
{
    public class Viajero
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(15)]
        public string Cedula { get; set; }

        [Required]
        [StringLength(300)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(25)]
        public string Telefono { get; set; }

        public List<ViajesRealizados> ViajesRealizados { get; set; }
    }
}
