﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgenciaApiEntity.ViajerosModels.AuxiliarModels
{
    public class ViajesCompletos
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Cedula { get; set; }

        public string Telefono { get; set; }

        public string CodigoViaje { get; set; }

        public string Origen { get; set; }

        public string Destino { get; set; }

        public double Precio { get; set; }
    }
}
