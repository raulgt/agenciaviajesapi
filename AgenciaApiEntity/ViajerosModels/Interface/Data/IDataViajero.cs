﻿using AgenciaApiEntity.ViajerosModels.Class;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiEntity.ViajerosModels.Interface.Data
{
    public interface IDataViajero
    {
        Task<List<Viajero>> GetViajeros();

        Task<Viajero> GetViajeroPorCedula(string cedula);

        Task<Viajero> GetViajero(int id);

        Task<bool> CreateViajero(Viajero viajero);

        Task<bool> DeleteViajero(int id);

        Task<bool> UpdateViajero(Viajero viajero);
       
    }
}
