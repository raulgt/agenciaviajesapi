﻿using AgenciaApiEntity.ViajerosModels.AuxiliarModels;
using AgenciaApiEntity.ViajerosModels.Class;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiEntity.ViajerosModels.Interface.Data
{
    public interface IDataViajesRealizados
    {

        Task<bool> CreateViajeRealizado(ViajesRealizados viajesRealizados);

        Task<bool> DeleteViajeRealizado(int id);

        Task<bool> UpdateViajeRealizado(ViajesRealizados viajesRealizados);

        Task<List<ViajesCompletos>> GetViajesRealizados();

        Task<ViajesRealizados> GetViajesRealizadosPorId(int id);

        Task<List<ViajesCompletos>> GetViajesRealizadosPorViajero(string cedula);
    }
}
