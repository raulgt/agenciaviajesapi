﻿using AgenciaApiEntity.ViajerosModels.Class;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiEntity.ViajerosModels.Interface.Service
{
    public interface IServiceViajesRealizados
    {

        Task<IOperationResult> CreateViajeRealizado(ViajesRealizados viajesRealizados);

        Task<IOperationResult> DeleteViajeRealizado(int id);

        Task<IOperationResult> UpdateViajeRealizado(ViajesRealizados viajesRealizados);

        Task<IOperationResult> GetViajesRealizados();

        Task<IOperationResult> GetViajesRealizadosPorId(int id);

        Task<IOperationResult> GetViajesRealizadosPorViajero(string cedula);
    }
}
