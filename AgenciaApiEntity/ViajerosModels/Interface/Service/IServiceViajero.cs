﻿using AgenciaApiEntity.ViajerosModels.Class;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiEntity.ViajerosModels.Interface.Service
{
    public interface IServiceViajero
    {       

        Task<IOperationResult> GetViajeros();

        Task<IOperationResult> GetViajeroPorCedula(string cedula);

        Task<IOperationResult> GetViajero(int Id);

        Task<IOperationResult> CreateViajero(Viajero viajero);

        Task<IOperationResult> DeleteViajero(int cedula);

        Task<IOperationResult> UpdateViajero(Viajero viajero);
    }
}
