﻿using AgenciaApiEntity;
using AgenciaApiEntity.ViajesModels.Class;
using AgenciaApiEntity.ViajesModels.Interface.Data;
using AgenciaApiEntity.ViajesModels.Interface.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiBussines.ViajesModels
{
    public class ServiceViaje : IServiceViaje
    {

        private readonly IOperationResult op;
        private readonly IDataViaje dataViaje;

        public ServiceViaje(IDataViaje dataViaje, IOperationResult op)
        {
            this.dataViaje = dataViaje;
            this.op = op;
        }

        public async Task<IOperationResult> CreateViaje(Viaje viaje)
        {
            try
            {
                 op.GetSuccessOperation(await dataViaje.CreateViaje(viaje));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> DeleteViaje(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataViaje.DeleteViaje(id));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViaje(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataViaje.GetViaje(id));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajes()
        {
            try
            {
                op.GetSuccessOperation(await dataViaje.GetViajes());
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> UpdateViaje(Viaje viaje)
        {
            try
            {
                op.GetSuccessOperation(await dataViaje.UpdateViaje(viaje));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }
    }
}
