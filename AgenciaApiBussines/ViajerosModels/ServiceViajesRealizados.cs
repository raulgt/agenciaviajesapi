﻿using AgenciaApiEntity;
using AgenciaApiEntity.ViajerosModels.Class;
using AgenciaApiEntity.ViajerosModels.Interface.Data;
using AgenciaApiEntity.ViajerosModels.Interface.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiBussines.ViajerosModels
{
    public class ServiceViajesRealizados : IServiceViajesRealizados
    {
        private readonly IOperationResult op;
        private readonly IDataViajesRealizados dataViajesRealizados;

        public ServiceViajesRealizados(IDataViajesRealizados dataViajesRealizados, IOperationResult op)
        {
            this.dataViajesRealizados = dataViajesRealizados;
            this.op = op;
        }

        public async Task<IOperationResult> CreateViajeRealizado(ViajesRealizados viajesRealizados)
        {
            try
            {
                op.GetSuccessOperation(await dataViajesRealizados.CreateViajeRealizado(viajesRealizados));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> DeleteViajeRealizado(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataViajesRealizados.DeleteViajeRealizado(id));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajesRealizados()
        {
            try
            {
                op.GetSuccessOperation(await dataViajesRealizados.GetViajesRealizados());
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajesRealizadosPorId(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataViajesRealizados.GetViajesRealizadosPorId(id));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajesRealizadosPorViajero(string cedula)
        {
            try
            {
                op.GetSuccessOperation(await dataViajesRealizados.GetViajesRealizadosPorViajero(cedula));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> UpdateViajeRealizado(ViajesRealizados viajesRealizados)
        {
            try
            {
                op.GetSuccessOperation(await dataViajesRealizados.UpdateViajeRealizado(viajesRealizados));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }
    }
}
