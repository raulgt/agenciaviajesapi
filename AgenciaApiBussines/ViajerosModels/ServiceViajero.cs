﻿using AgenciaApiEntity;
using AgenciaApiEntity.ViajerosModels.Class;
using AgenciaApiEntity.ViajerosModels.Interface.Data;
using AgenciaApiEntity.ViajerosModels.Interface.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaApiBussines.ViajerosModels
{
    public class ServiceViajero : IServiceViajero
    {
        private readonly IOperationResult op;
        private readonly IDataViajero dataViajero;

        public ServiceViajero(IDataViajero dataViajero, IOperationResult op)
        {
            this.dataViajero = dataViajero;
            this.op = op;
        }

        public async Task<IOperationResult> CreateViajero(Viajero viajero)
        {
            try
            {
                op.GetSuccessOperation(await dataViajero.CreateViajero(viajero));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> DeleteViajero(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataViajero.DeleteViajero(id));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajero(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataViajero.GetViajero(id));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajeroPorCedula(string cedula)
        {
            try
            {
                op.GetSuccessOperation(await dataViajero.GetViajeroPorCedula(cedula));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetViajeros()
        {
            try
            {
                op.GetSuccessOperation(await dataViajero.GetViajeros());
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> UpdateViajero(Viajero viajero)
        {
            try
            {
                op.GetSuccessOperation(await dataViajero.UpdateViajero(viajero));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

    }
}
